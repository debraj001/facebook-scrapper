
# Title:	Python Scraper to Download friends Data from Facebook
# Author:	Debraj Roy, University of Amsterdam
# Status:	Active
# Type:	Script
# Created:	30-September-2015
# Post-History:	30-September-2015

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from collections import defaultdict


data_dict = defaultdict(list)
driver = webdriver.Chrome()
driver.get('https://m.facebook.com/')
time.sleep(2)

wait = WebDriverWait(driver, 2)

username = driver.find_element_by_name('email')
time.sleep(2)
#add explicit wait

username.send_keys('xyz@gmail.com') #provide your email here
time.sleep(2)
#add explicit wait

password = driver.find_element_by_name('pass')
time.sleep(2)
#add explicit wait

password.send_keys('abcdefghi') #provide your password here
#add explicit wait

password.send_keys(Keys.RETURN)
time.sleep(2)
N=100 #No. of Friends/2
#switch to new window
driver.get("https://m.facebook.com/friends/center/friends/")

#scroll down the page iteratively with a delay
for _ in xrange(0, N):
    driver.execute_script("window.scrollBy(0, window.innerHeight);")
    time.sleep(2)
#Collect all links with the tag name - a
links = driver.find_elements_by_tag_name('a')
friends=[]
#parse the links to extract the link of all friends
for link in links:
    clas = link.get_attribute('class')
    if clas == 'darkTouch':
        friends.append(link.get_attribute('href'))
print "=======Downloaded ", len(friends), " friends========"

#for each friend collect the list of mutual friends
for links in friends:
    #switch to new window
    f= open("Fb_friend.txt","a+")
    f.write("\n")
    f.write(links)
    f.write(',')
    if str(links).find('profile.php?') != -1:
        url = str(links).replace('?','?v=friends&mutual=1&')
    else:
        url = links + "/friends?mutual=1"
    driver.get(url)
    for _ in xrange(0, 20):
        driver.execute_script("window.scrollBy(0, window.innerHeight);")
        time.sleep(2)

    mfriends = driver.find_elements_by_tag_name('a')

    for link in mfriends:
        clas = link.get_attribute('class')
        if clas == 'darkTouch':
            data_dict[links].append(link.get_attribute('href') or 0)
            f.write(link.get_attribute('href'))
            f.write(',')
print "Done!"
f.close()

